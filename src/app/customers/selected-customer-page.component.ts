import { Component } from '@angular/core';
import { SelectedCustomerPageService } from './selected-customer-page.service';

@Component({
  template: `<!-- 3. TODO Display the properties of the selected customer -->

    <ul *ngIf="customer$ | async as customer">
      <li>
        Customer Id: <span>{{ customer.id }}</span>
      </li>
      <li>
        Customer Name: <span>{{ customer.name }}</span>
      </li>
      <li>
        Customer Address: <span>{{ customer.address }}</span>
      </li>
      <li>
        Customer City: <span>{{ customer.city }}</span>
      </li>
      <li>
        Customer Country: <span>{{ customer.country }}</span>
      </li>
    </ul> `,
  providers: [SelectedCustomerPageService],
})
export class SelectedCustomerPageComponent {
  customer$ = this.facade.selectedCustomer$;

  constructor(private facade: SelectedCustomerPageService) {}
}
