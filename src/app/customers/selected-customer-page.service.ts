import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs';
import { Store } from '../store.service';

@Injectable()
export class SelectedCustomerPageService {
  selectedCustomer$ = this.store.appData$.pipe(
    switchMap(({ customers }) =>
      this.store.selectedCustomerId$.pipe(
        map((id) => customers.find((c) => c.id === id))
      )
    )
  );

  constructor(private store: Store) {}
}
