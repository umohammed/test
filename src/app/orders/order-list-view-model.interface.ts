import { AppData } from '../app-data/app-data.interface';
import { Order } from '../models/order.interface';

export interface OrderListViewModel extends Order {
  productName: string;
  customerName: string;
}

export function createOrderListViewModels({
  customers,
  orders,
  products,
}: AppData): OrderListViewModel[] {
  return orders.map((order) => ({
    ...order,
    customerName: customers.find((c) => c.id === order.customerId)?.name || '',
    productName: products.find((p) => p.id === order.productId)?.name || '',
  }));
}
