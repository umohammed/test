import { Customer } from '../models/customer.interface';

export interface CustomerSelectedViewModel extends Customer {
  selected: boolean;
}

export function createCustomerSelectedViewModels(
  customers: Customer[],
  selectedCustomerId: number | null
): CustomerSelectedViewModel[] {
  return customers.map((customer) => ({
    ...customer,
    selected: customer.id === selectedCustomerId,
  }));
}
