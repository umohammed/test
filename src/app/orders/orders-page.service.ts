import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs';
import { Store } from '../store.service';
import { createCustomerSelectedViewModels } from './customer-selected-view-model';
import { createOrderListViewModels } from './order-list-view-model.interface';

@Injectable()
export class OrdersPageService {
  private orders$ = this.store.appData$.pipe(map(createOrderListViewModels));

  customers$ = this.store.appData$.pipe(
    switchMap(({ customers }) =>
      this.store.selectedCustomerId$.pipe(
        map((id) => createCustomerSelectedViewModels(customers, id))
      )
    )
  );

  filteredOrders$ = this.orders$.pipe(
    switchMap((orders) =>
      this.store.selectedCustomerId$.pipe(
        map((id) =>
          id ? orders.filter((order) => order.customerId === id) : orders
        )
      )
    )
  );

  constructor(private store: Store) {}

  selectedCustomerChanged(select: HTMLSelectElement) {
    this.store.selectedCustomerChanged(+select.value);
  }
}
