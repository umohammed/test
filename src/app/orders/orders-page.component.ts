import { Component } from '@angular/core';
import { OrdersPageService } from './orders-page.service';

@Component({
  template: `
    <select (change)="onSelectChanged($event)">
      <!-- 2. TODO implement a select to filter orders by customer name -->
      <option [value]="null"></option>
      <option
        *ngFor="let customer of customers$ | async"
        [value]="customer.id"
        [selected]="customer.selected"
      >
        {{ customer.name }}
      </option>
    </select>
    <table>
      <thead>
        <th>Order Id</th>
        <th>Customer Name</th>
        <th>Order Date</th>
        <th>Product Name</th>
      </thead>
      <tbody>
        <!-- 1. TODO display a list of orders here. -->
        <tr *ngFor="let order of orders$ | async">
          <td>{{ order.id }}</td>
          <td>{{ order.customerName }}</td>
          <td>{{ order.date | date: 'dd/MM/yyy' }}</td>
          <td>{{ order.id }}</td>
        </tr>
      </tbody>
    </table>
  `,
  providers: [OrdersPageService],
})
export class OrdersPageComponent {
  orders$ = this.facade.filteredOrders$;

  customers$ = this.facade.customers$;

  constructor(private facade: OrdersPageService) {}

  onSelectChanged(event: Event) {
    this.facade.selectedCustomerChanged(event.target as HTMLSelectElement);
  }
}
