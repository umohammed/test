import { Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin, map, shareReplay } from 'rxjs';
import { AppDataApiService } from './app-data/app-data-api.service';

@Injectable({ providedIn: 'root' })
export class Store {
  appData$ = forkJoin([
    this.appData.customers$,
    this.appData.orders$,
    this.appData.products$,
  ]).pipe(
    map(([customers, orders, products]) => ({ customers, orders, products })),
    shareReplay(1)
  );

  private selectedCustomerIdSubject = new BehaviorSubject<number | null>(null);

  selectedCustomerId$ = this.selectedCustomerIdSubject.asObservable();

  constructor(private appData: AppDataApiService) {}

  selectedCustomerChanged(id: number) {
    this.selectedCustomerIdSubject.next(id);
  }
}
